// @flow
export const init = () => {
    const clock = new Clock();

    console.info('The time is ', clock.getTime());
    console.info('The date is ', clock.getDate());
    console.info('The date is ', Clock.getDate());
};

function Clock() {
    // Constructor function
}

/**
 * This function is added to the "class" via its prototype.
 * Prototypes affect all instances of the object.
 */
Clock.prototype.getTime = () => Date.now();

/**
 * This function does not get added to instances of the class.
 * This can be considered a static method.
 */
Clock.getDate = () => Date.now();

/**
 * ES2015 class.
 * This is the same, as using ES5 prototypes.
 */
class Watch {

    getTime() {
        return Date.now();
    }

    static getDate() {
        return Date.now();
    }
}
