// @flow
const numbers: Array<number> = [1, 1, 2, 3, 5, 8, 13, 21];

export const init = () => {
    /**
     * Array.prototype.forEach
     *
     * Iterates over an array.
     */
    numbers.forEach((n: number) => {
        console.info(n);
    });

    /**
     * Array.prototype.map
     *
     * Iterates over an array and returns a new array.
     * @type {Array}
     */
    const double = numbers.map((n: number) =>  n * 2);
    console.info('map', double);

    /**
     * Array.prototype.filter
     *
     * Iterates over an array and returns a new array matching the predicate.
     *
     * Predicate:
     * A function, that returns true.
     *
     * e.g. number % 2 === 0
     *
     * @type {Array.<number>}
     */
    const even = numbers.filter((n: number) => n % 2 === 0);
    console.info('filter', even);

    /**
     * Array.prototype.some
     *
     * Iterates over an array and returns, if at least one element matches the predicate.
     *
     * @type {boolean}
     */
    const hasEvenNumbers = numbers.some((n: number) => n % 2 === 0);
    console.info('some', hasEvenNumbers);

    /**
     * Array.prototype.every
     *
     * Iterates over an array and returns, if all elements match the predicate.
     *
     * @type {boolean}
     */
    const hasAllEvenNumbers = numbers.every((n: number) => n % 2 === 0);
    console.info('every', hasAllEvenNumbers);

    /**
     * Array.prototype.reduce
     *
     * Iterates over an array and calls a function receiving a result parameter and the array item.
     * The initial value of the result can be set via the last parameter.
     *
     * In the end, the result parameter gets returned from the function.
     *
     * It can be used to filter, map, sum, convert arrays to objects and much more.
     */
    const sum = numbers.reduce((sum: number, n: number) => {
        return sum + n;
    }, 0);
    console.info('reduce', sum);
};
