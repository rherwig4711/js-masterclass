// @flow
function double(n: number) {
    return n * 2;
}

/**
 * Functions can be written as 'arrow functions' by omitting the function keyword and adding a
 * fat arrow `=>`.
 *
 * @param n
 * @returns {number}
 */
const double2 = (n: number) => {
    return n * 2;
};

/**
 * If the function takes exactly 1 parameter, the parameter parentheses can be omitted.
 * This is only possible, when not using flow types.
 * @param n
 */
const double3 = n => {
    return n * 2;
};

/**
 * If the function only consists of a return statement, the curly braces can be omitted.
 * @param n
 */
const double4 = n => n * 2;
