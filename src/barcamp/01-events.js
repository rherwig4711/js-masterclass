// @flow
const $button01: ?EventTarget = document.querySelector('#button01');
const $button02: ?EventTarget = document.querySelector('#button01');
const $link: ?EventTarget = document.querySelector('#link');
const $container: ?EventTarget = document.querySelector('.container');

export const init = () => {
    if (!$button01 || !$button02 || !$container || !$link) {
        return;
    }

    /**
     * addEventListener
     *
     * eventType: string
     * callback: () => void
     * options?: Object
     *
     * See list of events at
     * https://developer.mozilla.org/de/docs/Web/API/EventTarget/addEventListener
     */
    $button01.addEventListener('click', (event: Event) => {
        console.info('This is a standard event listener.', event.target);
    }, {
        once: false, // removes listener after it triggered once
        passive: true // tells the event listener, that `preventDefault()` is never called
    });

    /**
     * Bubbling
     *
     * Events "bubble up" the DOM tree and trigger on all parents by default.
     */
    $container.addEventListener('click', (event: Event) => {
        console.info('This click bubbles up from the button event.', event.target);
    });

    /**
     * To prevent bubbling, call `event.stopPropagation()`.
     */
    $button02.addEventListener('click', (event: Event) => {
        event.stopPropagation();
        console.info('This event prents bubbling.', event.target);
    });

    /**
     * `preventDefault()` stops the default behavior of the event.
     * In this example, the links href does not get called.
     */
    $link.addEventListener('click', (event: Event) => {
        event.preventDefault();
        console.info('This link does not work', event.target);
    });

};
