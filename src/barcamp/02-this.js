// @flow
export const init = () => {
    const obj = {
        test() {
            console.info('This in test', this);
        },
        testFunction: function() {
            console.info('This in testFunction', this);
        },
        testArrow: () => {
            console.info('This in testArrow', this);
        }
    };

    obj.test(); // ?
    obj.testFunction(); // ?
    obj.testArrow(); // ?

    const test = obj.test;
    test(); // ?

    // Assign different this context with `apply`, `call` or `bind`.
    // test.call(window);
    // test.apply(window);
    // test.bind(window); // Function does not get called using bind!
};
