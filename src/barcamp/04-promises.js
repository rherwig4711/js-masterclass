// @flow
export const init = () => {
    /**
     * After a promise is resolved, the `then` function gets called.
     */
    apiCall().then(() => {
        console.info('apiCall resolved.');
    });

    /**
     * This promise gets rejected. In this case, the `catch` method triggers.
     */
    apiCall(false).then(() => {
        console.info('apiCall resolved.')
    }).catch((err) => {
        console.error('Call failed', err);
    });

    getApiResult();
};

async function getApiResult() {
    try {
        const result = await apiCall();
        console.info('apiCall succeeded', result);
    } catch(e) {
        console.error('Request failed', e);
    }
}

/**
 * This function simulates an API call that takes 1 second to complete.
 *
 * It can either fail or succeed, depending on the parameter passed.
 *
 * @returns {Promise}
 */
function apiCall(test = true) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return test ? resolve({ data: [] }) : reject({ message: 'call failed' });
        }, 1000);
    });
}
