// @flow
const multiply = (n: number, m: number) => n * m;
const add = (n: number, m: number) => n + m;

/**
 * If we want to add 2 to a number and then multiply it by five, we needed to do the following:
 */
let number = 2;
number = add(number, 2);
number = multiply(number, 5);

// number === 20
// or ...

number = multiply(add(number, 2), 5);

/**
 * Currying means, that a function returns another function.
 * This is used to compose functions.
 */

const _multiply = m => n => n * m;
const multiplyBy5 = _multiply(5);

number = 2;
number = multiplyBy5(number);

// number === 1
