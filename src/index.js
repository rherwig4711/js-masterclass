import './global.css';

import 'babel-polyfill';

import { init as init01 } from './barcamp/01-events';
import { init as init02 } from './barcamp/02-this';
import { init as init03 } from './barcamp/03-prototypes';
import { init as init04 } from './barcamp/04-promises';
import { init as init05 } from './barcamp/05-array';

if (module.hot) {
    module.hot.accept();
}

global.BarCamp = {
    init01,
    init02,
    init03,
    init04,
    init05
};
