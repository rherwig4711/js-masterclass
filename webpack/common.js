const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const join = require('path').join;

const ROOT_DIR = join(__dirname, '..');

module.exports = {
    context: join(ROOT_DIR, 'src'),
    entry: [
        './index'
    ],
    output: {
        path: join(ROOT_DIR, 'build'),
        filename: 'js-masterclass.js'
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [{
            test: /\.js$/,
            use: 'babel-loader'
        }, {
            test: /\.css$/,
            use: ['css-hot-loader'].concat(ExtractTextWebpackPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        modules: true,
                        importLoaders: 1,
                        localIdentName: '[name]__[local]___[hash:base64:5]'
                    }
                }, {
                    loader: 'postcss-loader',
                    options: {
                        config: {
                            path: join(__dirname, '../postcss.config.js')
                        }
                    }
                }]
            }))
        }, {
            test: /\.(jpg|png|gif)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]'
                }
            }]
        }]
    },
    plugins: [
        new ExtractTextWebpackPlugin('js-masterclass.css'),
        new HtmlWebpackPlugin({
            template: join(ROOT_DIR, 'src/index.html'),
            filename: 'index.html'
        })
    ]
};
