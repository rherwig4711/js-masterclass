const webpack = require('webpack');
const merge = require('webpack-merge');
const commonConfig = require('./common');

module.exports = merge(commonConfig, {
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:8080'
    ],
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        historyApiFallback: true,
        hot: true
    },
    devtool: '#inline-source-map',
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
});
